/*


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"flag"
	"fmt"
	"os"

	"gitlab.com/gitlab-org/gl-openshift/gitlab-runner-operator/controllers/manager"
	_ "k8s.io/client-go/plugin/pkg/client/auth/gcp"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"

	appsv1beta2 "gitlab.com/gitlab-org/gl-openshift/gitlab-runner-operator/api/v1beta2"
	"gitlab.com/gitlab-org/gl-openshift/gitlab-runner-operator/controllers"
	// +kubebuilder:scaffold:imports
)

var (
	buildVersion string
	buildDate    string
)

func main() {
	var version bool
	var metricsAddr string
	var enableLeaderElection bool
	flag.BoolVar(&version, "version", false, "Prints the build version of the manager")
	flag.StringVar(&metricsAddr, "metrics-addr", ":8080", "The address the metric endpoint binds to.")
	flag.BoolVar(&enableLeaderElection, "enable-leader-election", false,
		"Enable leader election for controller manager. "+
			"Enabling this will ensure there is only one active controller manager.")

	flag.Parse()

	fmt.Printf("GitLab Runner Operator Version: %s (%s)\n", buildVersion, buildDate)

	if version {
		return
	}

	setupLog := ctrl.Log.WithName("setup")
	ctrl.SetLogger(zap.New(zap.UseDevMode(true)))

	mgr, err := manager.New(
		manager.WithMetricsAddr(metricsAddr),
		manager.WithLeaderElection(enableLeaderElection),
	)
	if mgr.Opts.Namespace == "" {
		ctrl.Log.Info("Watching all namespaces")
	} else {
		ctrl.Log.WithValues("watchNamespace", mgr.Opts.Namespace).Info("Watching namespace")
	}

	if _, err = controllers.New(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "Runner")
		os.Exit(1)
	}

	if os.Getenv("ENABLE_WEBHOOKS") != "false" {
		if err = (&appsv1beta2.Runner{}).SetupWebhookWithManager(mgr); err != nil {
			setupLog.Error(err, "unable to create webhook", "webhook", "Runner")
			os.Exit(1)
		}
	}

	setupLog.Info("starting manager")
	if err := mgr.Start(ctrl.SetupSignalHandler()); err != nil {
		setupLog.Error(err, "problem running manager")
		os.Exit(1)
	}
}
