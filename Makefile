SHELL=/bin/bash

# Current Operator version
VERSION ?= v0.0.1
# Even if an empty VERSION is passed we override it
# since we always need VERSION. This is useful for dev builds
# of the operator and bundle images
ifeq ($(VERSION),)
override VERSION := v0.0.1
endif

# The Revision is used for unofficial builds since we need a semver-compatible
# version for the bundle
ifneq ($(REVISION),)
override VERSION := $(VERSION)-$(REVISION)
endif

PREVIOUS_VERSION ?= $(shell mage versions:previous "$(VERSION)")

# Current GitLab Runner version
APP_VERSION ?= v$(shell cat APP_VERSION)

GITLAB_RUNNER_OPERATOR_REGISTRY ?= registry.gitlab.com/gitlab-org/gl-openshift/gitlab-runner-operator

GITLAB_RUNNER_REGISTRY ?= registry.gitlab.com/gitlab-org/ci-cd/gitlab-runner-ubi-images
GITLAB_RUNNER_IMAGE ?= $(GITLAB_RUNNER_REGISTRY)/gitlab-runner-ocp:$(APP_VERSION)
GITLAB_RUNNER_HELPER_IMAGE ?= $(GITLAB_RUNNER_REGISTRY)/gitlab-runner-helper-ocp:$(APP_VERSION)

K8S_RBAC_PROXY_IMAGE_REGISTRY ?= $(GITLAB_RUNNER_OPERATOR_REGISTRY)/openshift4/ose-kube-rbac-proxy
K8S_RBAC_PROXY_IMAGE_VERSION ?= 4.15.0
K8S_RBAC_PROXY_IMAGE ?= $(K8S_RBAC_PROXY_IMAGE_REGISTRY):v$(K8S_RBAC_PROXY_IMAGE_VERSION)

# Default bundle image tag
BUNDLE_IMG ?= $(GITLAB_RUNNER_OPERATOR_REGISTRY)/gitlab-runner-operator-bundle:$(VERSION)

# Image URL to use all building/pushing image targets
IMG ?= $(GITLAB_RUNNER_OPERATOR_REGISTRY)/gitlab-runner-operator:$(VERSION)
IMG_ARCH ?= $(GITLAB_RUNNER_OPERATOR_REGISTRY)/gitlab-runner-operator:$(ARCH)-$(VERSION)
IMG_ARCH_TEMPLATE ?= $(GITLAB_RUNNER_OPERATOR_REGISTRY)/gitlab-runner-operator:ARCH-$(VERSION)

# Image for catalog source
CATALOG_SOURCE_IMG ?= $(GITLAB_RUNNER_OPERATOR_REGISTRY)/gitlab-runner-operator-catalog-source:$(VERSION)
CATALOG_SOURCE_IMG_ARCH ?= $(GITLAB_RUNNER_OPERATOR_REGISTRY)/gitlab-runner-operator-catalog-source:$(ARCH)-$(VERSION)
CATALOG_SOURCE_IMG_ARCH_TEMPLATE ?= $(GITLAB_RUNNER_OPERATOR_REGISTRY)/gitlab-runner-operator-catalog-source:ARCH-$(VERSION)

# OPM Image
OPM_IMAGE ?= $(GITLAB_RUNNER_OPERATOR_REGISTRY)/openshift4/ose-operator-registry
OPM_IMAGE_ARCH ?= $(GITLAB_RUNNER_OPERATOR_REGISTRY)/openshift4/ose-operator-registry:$(ARCH)
OPM_IMAGE_ARCH_TEMPLATE ?= $(GITLAB_RUNNER_OPERATOR_REGISTRY)/openshift4/ose-operator-registry:ARCH

# Options for building bundles
BUNDLE_CHANNELS ?= stable
BUNDLE_DEFAULT_CHANNEL ?= stable
BUNDLE_METADATA_OPTS ?= --channels=$(BUNDLE_CHANNELS) --default-channel=$(BUNDLE_DEFAULT_CHANNEL)
BUNDLE_GEN_FLAGS ?= --verbose --use-image-digests --manifests --metadata --overwrite $(BUNDLE_METADATA_OPTS) --version $(subst v,,$(VERSION))

TEMPLATE_VARS := VERSION=$(subst v,,$(VERSION))
TEMPLATE_VARS += APP_VERSION=$(subst v,,$(APP_VERSION))
TEMPLATE_VARS += PREVIOUS_VERSION=$(subst v,,$(PREVIOUS_VERSION))
TEMPLATE_VARS += OPERATOR_IMAGE=$(IMG)
TEMPLATE_VARS += GITLAB_RUNNER_OPERATOR_REGISTRY=$(GITLAB_RUNNER_OPERATOR_REGISTRY)
TEMPLATE_VARS += GITLAB_RUNNER_IMAGE=$(GITLAB_RUNNER_IMAGE)
TEMPLATE_VARS += GITLAB_RUNNER_HELPER_IMAGE=$(GITLAB_RUNNER_HELPER_IMAGE)
TEMPLATE_VARS += K8S_RBAC_PROXY_IMAGE_REGISTRY=$(K8S_RBAC_PROXY_IMAGE_REGISTRY)
TEMPLATE_VARS += K8S_RBAC_PROXY_IMAGE_VERSION=$(K8S_RBAC_PROXY_IMAGE_VERSION)
TEMPLATE_VARS += K8S_RBAC_PROXY_IMAGE=$(K8S_RBAC_PROXY_IMAGE)

KUSTOMIZE_BUILD_OPTS ?= --load-restrictor LoadRestrictionsNone

PLATFORMS ?= linux/amd64,linux/ppc64le
PLATFORM ?= linux/amd64
ARCH ?= amd64
OS ?= linux

BINDIR = $(shell pwd)/bin
$(shell mkdir -p $(BINDIR))

MACHINE_NAME ?= $(shell uname -m)
HOST_ARCH ?= $(MACHINE_NAME)
ifeq ($(HOST_ARCH),x86_64)
override HOST_ARCH := amd64
endif

HOST_OS ?= $(shell uname | tr '[:upper:]' '[:lower:]')

PIP ?= python3 -m pip

_install_go_binary: BINARY_VERSION ?=
_install_go_binary: DOWNLOAD_URL ?=
_install_go_binary: export GOBIN = $(BINDIR)
_install_go_binary:
	# Installing $(DOWNLOAD_URL)@$(BINARY_VERSION)
	go install $(DOWNLOAD_URL)@$(BINARY_VERSION)

MOCKERY ?= $(BINDIR)/mockery
$(MOCKERY):
	$(MAKE) _install_go_binary BINARY_VERSION=v2.42.2 DOWNLOAD_URL=github.com/vektra/mockery/v2

CONTROLLER_GEN ?= $(BINDIR)/controller-gen
$(CONTROLLER_GEN):
	$(MAKE) _install_go_binary BINARY_VERSION=v0.14.0 DOWNLOAD_URL=sigs.k8s.io/controller-tools/cmd/controller-gen

GOLINT ?= $(BINDIR)/golint
$(GOLINT):
	$(MAKE) _install_go_binary BINARY_VERSION=master DOWNLOAD_URL=golang.org/x/lint/golint

SKOPEO ?= $(BINDIR)/skopeo
$(SKOPEO):
	@BINDIR=$(BINDIR) HOST_OS=$(HOST_OS) ./hack/scripts/install_skopeo.sh

GITLAB_CHANGELOG = $(BINDIR)/gitlab-changelog
$(GITLAB_CHANGELOG): GITLAB_CHANGELOG_VERSION = master
$(GITLAB_CHANGELOG): ARCH = amd64# On MacOS this will use Rosetta in case amd64 is used. This version is not supported for arm64 and the naming schemes for newer versions are different.
$(GITLAB_CHANGELOG): DOWNLOAD_URL = https://storage.googleapis.com/gitlab-runner-tools/gitlab-changelog/$(GITLAB_CHANGELOG_VERSION)/gitlab-changelog-$(HOST_OS)-$(ARCH)
$(GITLAB_CHANGELOG):
	# Installing $(DOWNLOAD_URL) as $(GITLAB_CHANGELOG)
	@curl -sL $(DOWNLOAD_URL) -o $(GITLAB_CHANGELOG)
	@chmod +x $(GITLAB_CHANGELOG)

OPERATOR_SDK ?= $(BINDIR)/operator-sdk
$(OPERATOR_SDK): OPERATOR_SDK_VERSION = v1.34.1
$(OPERATOR_SDK): ARCH = amd64# On MacOS this will use Rosetta in case amd64 is used. This version is not supported for arm64 and the naming schemes for newer versions are different.
$(OPERATOR_SDK): DOWNLOAD_URL = https://github.com/operator-framework/operator-sdk/releases/download/$(OPERATOR_SDK_VERSION)/operator-sdk_$(HOST_OS)_$(ARCH)
$(OPERATOR_SDK): DIR = $(shell dirname $(OPERATOR_SDK))
$(OPERATOR_SDK):
	# Installing $(DOWNLOAD_URL) as $(OPERATOR_SDK)
	@curl -sL $(DOWNLOAD_URL) -o $(OPERATOR_SDK)
	@chmod +x $(OPERATOR_SDK)

YQ ?= $(BINDIR)/yq
$(YQ): YQ_VERSION = v4.30.5
$(YQ): ARCH = amd64# On MacOS this will use Rosetta in case amd64 is used. This version is not supported for arm64 and the naming schemes for newer versions are different.
$(YQ): DOWNLOAD_URL = https://github.com/mikefarah/yq/releases/download/$(YQ_VERSION)/yq_$(HOST_OS)_$(ARCH)
$(YQ):
	# Installing $(DOWNLOAD_URL) as $(YQ)
	@curl -sL $(DOWNLOAD_URL) -o $(YQ)
	@chmod +x $(YQ)

OPM_HOST_OS := $(HOST_OS)
ifeq ($(OPM_HOST_OS),darwin)
override OPM_HOST_OS := mac
endif

OPM ?= $(BINDIR)/opm
$(OPM): OPM_VERSION = 4.6.60
$(OPM): ARCH = amd64# On MacOS this will use Rosetta in case amd64 is used. This version is not supported for arm64 and the naming schemes for newer versions are different.
$(OPM): ARCHIVE_NAME = opm-$(OPM_HOST_OS)-$(OPM_VERSION).tar.gz
$(OPM): DOWNLOAD_URL = https://mirror.openshift.com/pub/openshift-v4/$(ARCH)/clients/ocp/$(OPM_VERSION)/$(ARCHIVE_NAME)
$(OPM):
	# Installing $(DOWNLOAD_URL) as $(OPM)
	@curl -sL $(DOWNLOAD_URL) -o $(BINDIR)/$(ARCHIVE_NAME)
	@tar -C $(BINDIR) -zxvf $(BINDIR)/$(ARCHIVE_NAME)
	@# The darwin archive contains a platform specific binary while the linux archive does not
	@mv $(BINDIR)/$(HOST_OS)-$(ARCH)-opm $(OPM) || true
	@chmod +x $(OPM)
	@rm -f $(BINDIR)/$(ARCHIVE_NAME)

KUSTOMIZE ?= $(BINDIR)/kustomize
$(KUSTOMIZE): KUSTOMIZE_VERSION = v5.3.0
$(KUSTOMIZE): ARCH = amd64# On MacOS this will use Rosetta in case amd64 is used. This version is not supported for arm64 and the naming schemes for newer versions are different.
$(KUSTOMIZE): ARCHIVE_NAME = kustomize_$(KUSTOMIZE_VERSION)_$(HOST_OS)_$(ARCH).tar.gz
$(KUSTOMIZE): DOWNLOAD_URL = https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize%2F$(KUSTOMIZE_VERSION)/$(ARCHIVE_NAME)
$(KUSTOMIZE):
	# Installing $(DOWNLOAD_URL) as $(KUSTOMIZE)
	@curl -sL $(DOWNLOAD_URL) -o $(BINDIR)/$(ARCHIVE_NAME)
	@tar -C $(BINDIR) -zxvf $(BINDIR)/$(ARCHIVE_NAME)
	@chmod +x $(KUSTOMIZE)
	@rm -f $(BINDIR)/$(ARCHIVE_NAME)

MANIFEST_TOOL ?= $(BINDIR)/manifest-tool
$(MANIFEST_TOOL):
	$(MAKE) _install_go_binary BINARY_VERSION=8b310e938a67c0787fbbd679b89d7251287c7c24 DOWNLOAD_URL=github.com/estesp/manifest-tool/v2/cmd/manifest-tool

MAGE ?= $(BINDIR)/mage
$(MAGE):
	cd $(BINDIR) && \
	rm -rf mage && \
	git clone https://github.com/magefile/mage && \
	cd mage && \
	go run bootstrap.go && \
	cd .. && \
	rm -rf mage && \
	cp $(shell which mage) .

tools:
	$(MAKE) $(GITLAB_CHANGELOG) $(MOCKERY) $(OPERATOR_SDK) $(YQ) $(OPM) $(KUSTOMIZE) $(CONTROLLER_GEN) $(SKOPEO) $(MAGE) $(MANIFEST_TOOL)

all: manager

# Run tests
test:
	go test -v ./...

integration_test:
	go test -v ./... -tags=integration

lint: $(GOLINT)
lint:
	$(GOLINT) -set_exit_status $(shell go list ./... | grep -v /vendor | grep -v /magefiles)

# Run go fmt against code
fmt:
	go fmt ./...

# Run go vet against code
vet:
	go vet ./...

# Build manager binary
manager: fmt vet
	go build -o bin/manager main.go

# Run against the configured Kubernetes cluster in ~/.kube/config
run: fmt vet
	go run ./main.go

# Install CRDs into a cluster
install: manifests $(KUSTOMIZE)
	$(KUSTOMIZE) build config/crd | kubectl apply -f -

# Uninstall CRDs from a cluster
uninstall: manifests $(KUSTOMIZE)
	$(KUSTOMIZE) build config/crd | kubectl delete -f -

# Deploy controller in the configured Kubernetes cluster in ~/.kube/config
deploy: manifests $(KUSTOMIZE)
	cd config/manager && $(KUSTOMIZE) edit set image controller=$(IMG)
	$(KUSTOMIZE) build config/default | kubectl apply -f -

undeploy: manifests $(KUSTOMIZE)
	$(KUSTOMIZE) build config/default | kubectl delete -f -

# Generate manifests e.g. CRD, RBAC etc.
.PHONY: generate-crd
generate-crd: $(CONTROLLER_GEN)
generate-crd: CRDS_DIR ?= config/crd/bases
generate-crd:
	$(CONTROLLER_GEN) crd:ignoreUnexportedFields=true rbac:roleName=manager-role paths="./..." output:crd:artifacts:config=$(CRDS_DIR)

.PHONY: clean-bundle
clean-bundle:
	rm -rf bundle/manifests
	mkdir -p bundle/manifests

# Install CRDs into a cluster
install-k8s: manifests-k8s $(KUSTOMIZE)
	$(KUSTOMIZE) build config/crd | kubectl apply -f -

# Uninstall CRDs from a cluster
uninstall-k8s: manifests-k8s $(KUSTOMIZE)
	$(KUSTOMIZE) build config/crd | kubectl delete -f -

install-bundle:
	kubectl apply -f bundle/manifests

uninstall-bundle:
	kubectl delete -f bundle/manifests

# Deploy controller in the configured Kubernetes cluster in ~/.kube/config
.PHONY: deploy-k8s
deploy-k8s: BUNDLE_MANIFESTS_DIR ?= bundle/manifests
deploy-k8s:
	find $(BUNDLE_MANIFESTS_DIR) -name "*namespace*" |xargs kubectl apply -f
	kubectl apply -f $(BUNDLE_MANIFESTS_DIR)

.PHONY: undeploy-k8s
undeploy-k8s: BUNDLE_MANIFESTS_DIR ?= bundle/manifests
undeploy-k8s:
	kubectl delete -f $(BUNDLE_MANIFESTS_DIR)

.PHONY: manifests-prepare
manifests-prepare: $(KUSTOMIZE) $(YQ) $(MAGE)
manifests-prepare: CONFIG_MANIFESTS_DIR ?= config/manifests
manifests-prepare: NAMESPACE ?= gitlab-runner-system
manifests-prepare: NAMEPREFIX ?= gitlab-runner-
manifests-prepare: TEMPLATE_TARGET ?=
manifests-prepare:
	$(MAKE) generate-crd

	$(MAGE) template:configManifest $(TEMPLATE_TARGET) "$(TEMPLATE_VARS)"

	cd config/default && \
		$(KUSTOMIZE) edit set namespace $(NAMESPACE) && \
		$(KUSTOMIZE) edit set nameprefix $(NAMEPREFIX)

# Generate bundle manifests and metadata, then validate generated files.
.PHONY: manifests-k8s
manifests-k8s: $(KUSTOMIZE) $(OPERATOR_SDK)
manifests-k8s: export TEMPLATE_TARGET = kubernetes
manifests-k8s: MANIFESTS_OUTPUT ?= operator.k8s.yaml
manifests-k8s: CONFIG_MANIFESTS_DIR ?= config/manifests
manifests-k8s:
	$(MAKE) manifests-prepare

	$(KUSTOMIZE) build $(KUSTOMIZE_BUILD_OPTS) $(CONFIG_MANIFESTS_DIR) -o $(MANIFESTS_OUTPUT)

.PHONY: bundle
bundle: $(KUSTOMIZE) $(OPERATOR_SDK)
bundle: export TEMPLATE_TARGET = kubernetes-olm
bundle: BUNDLE_OUTPUT ?= bundle
bundle: CONFIG_MANIFESTS_DIR ?= config/manifests
bundle: OPERATOR_SDK_OUTPUT_FLAGS ?= --output-dir $(BUNDLE_OUTPUT)
bundle: $(YQ)
bundle:
	$(MAKE) manifests-prepare

	rm -rf $(BUNDLE_OUTPUT)

	$(KUSTOMIZE) build $(KUSTOMIZE_BUILD_OPTS) $(CONFIG_MANIFESTS_DIR) \
		| $(OPERATOR_SDK) generate bundle $(BUNDLE_GEN_FLAGS) $(OPERATOR_SDK_OUTPUT_FLAGS)

	# TODO: I don't like these post processing steps, but they are necessary for now
	# The annotations.yaml seems to not be intended to be modified by the operator-sdk automation
	$(YQ) -i '.annotations."com.redhat.openshift.versions" = "$(shell cat OPENSHIFT_VERSIONS)"' $(BUNDLE_OUTPUT)/metadata/annotations.yaml

.PHONY: manifests-k8s-olm
manifests-k8s-olm: $(YQ)
manifests-k8s-olm: export MANIFESTS_OUTPUT ?= operator.yaml
manifests-k8s-olm: export BUNDLE_OUTPUT ?= bundle
manifests-k8s-olm:
	$(MAKE) bundle
	$(YQ) e '.' $(BUNDLE_OUTPUT)/manifests/* > $(MANIFESTS_OUTPUT)

# Build the docker image
.PHONY: docker-build
docker-build:
	docker buildx build --platform "$(PLATFORM)" -t $(IMG_ARCH) --output type=image --build-arg ARCH=$(ARCH) --build-arg OS=$(OS) --build-arg VERSION=$(VERSION) .

.PHONY: docker-push
docker-push:
	mage docker:push $(IMG_ARCH)

.PHONY: docker-manifest-operator
docker-manifest-operator: $(MANIFEST_TOOL)
docker-manifest-operator:
	$(MANIFEST_TOOL) push from-args \
		--platforms $(PLATFORMS) \
		--template $(IMG_ARCH_TEMPLATE) \
		--target $(IMG)

# Build the bundle image.
.PHONY: docker-bundle-build
docker-bundle-build:
	docker build -f bundle.Dockerfile -t $(BUNDLE_IMG) .

.PHONY: docker-bundle-push
docker-bundle-push:
	mage docker:push $(BUNDLE_IMG)

.PHONY: docker-catalog-build
docker-catalog-build: $(OPM)
	$(OPM) index add --bundles $(BUNDLE_IMG) --mode semver --tag $(CATALOG_SOURCE_IMG_ARCH) --build-tool docker \
		--binary-image $(OPM_IMAGE_ARCH)

.PHONY: docker-catalog-push
docker-catalog-push:
	mage docker:push $(CATALOG_SOURCE_IMG_ARCH)

.PHONY: docker-catalog-manifest
docker-catalog-manifest: $(MANIFEST_TOOL)
docker-catalog-manifest:
	$(MANIFEST_TOOL) push from-args \
		--platforms $(PLATFORMS) \
		--template $(CATALOG_SOURCE_IMG_ARCH_TEMPLATE) \
		--target $(CATALOG_SOURCE_IMG)

.PHONY: mocks
mocks: $(MOCKERY)
mocks: export PATH=$(BINDIR):$(shell printenv PATH)
mocks:
	find . -type f -name 'mock_*' -delete
	go generate ./...

check_mocks: mocks
	# Checking the differences
	@git --no-pager diff --exit-code -- ./helpers/service/mocks \
		$(shell git ls-files | grep 'mock_' | grep -v 'vendor/') && \
		!(git ls-files -o | grep 'mock_' | grep -v 'vendor/') && \
		echo "Mocks up-to-date!"

# Generate code
generate: $(CONTROLLER_GEN)
	$(CONTROLLER_GEN) object paths="./..."

check_generated_objects: generate
	# Checking the differences
	@git --no-pager diff --exit-code -- ./api/v1beta2 \
		$(shell git ls-files | grep 'zz_generated') && \
		!(git ls-files -o | grep 'zz_generated') && \
		echo "controller-gen objects up-to-date!"

.PHONY: generate_changelog
generate_changelog: export CHANGELOG_RELEASE ?= dev
generate_changelog: $(GITLAB_CHANGELOG)
	# Generating new changelog entries
	@$(GITLAB_CHANGELOG) -project-id 22848448 \
		-release $(CHANGELOG_RELEASE) \
		-starting-point-matcher "v[0-9]*.[0-9]*.[0-9]*" \
		-config-file .gitlab/changelog.yml \
		-changelog-file CHANGELOG.md
