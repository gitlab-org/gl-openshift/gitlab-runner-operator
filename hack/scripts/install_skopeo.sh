#!/bin/bash

if [ "$HOST_OS" == "darwin" ]; then
		if ! which skopeo > /dev/null; then
			brew install skopeo
		fi
		cp $(which skopeo) "$BINDIR/skopeo"
else
  echo "WARNING:"
  echo "WARNING: Skopeo requires manual installation. Please see https://github.com/containers/skopeo/blob/main/install.md"
  echo "WARNING:"
fi