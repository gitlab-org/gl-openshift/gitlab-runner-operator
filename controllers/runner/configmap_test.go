package runner

import (
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestRenderConfigToml(t *testing.T) {
	tests := map[string]struct {
		cfg      Config
		expected string
	}{
		"default config": {
			cfg: newConfigDefault(),
			expected: `concurrent = 10
check_interval = 30
log_level = "info"
listen_address = "[::]:9252"
`,
		},
		"custom config": {
			cfg: Config{
				Concurrent:       5,
				CheckInterval:    60,
				ShutdownTimeout:  15,
				LogLevel:         "debug",
				LogFormat:        "json",
				SentryDSN:        "https://sentry.example.com",
				ConnectionMaxAge: "5m",
				ListenAddr:       ":9000",
			},
			expected: `concurrent = 5
check_interval = 60
log_level = "debug"
listen_address = ":9000"
shutdown_timeout = 15
log_format = "json"
sentry_dsn = "https://sentry.example.com"
connection_max_age = "5m"
`,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			pwd, err := os.Getwd()
			require.NoError(t, err)
			templatesPath := filepath.Join(pwd, "../../hack/assets/templates/")

			result := renderConfigToml(templatesPath, tt.cfg)
			assert.Equal(t, tt.expected, result)
		})
	}
}
