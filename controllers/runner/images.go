package runner

import (
	"os"
)

type imageName string

const (
	// GitLabRunnerImage has name of runner
	GitLabRunnerImage = imageName("gitlab-runner")
	// GitLabRunnerHelperImage has name of runner helper
	GitLabRunnerHelperImage = imageName("gitlab-runner-helper")
	// releaseFile is the location where metadata about releases is kept. Look at hack/assets
	releaseFile = "release.yaml"
)

//go:generate mockery --name=imageResolver --inpackage
type imageResolver interface {
	HelperImage() (string, error)
	RunnerImage() (string, error)
}

type envImageResolver struct {
}

func (e envImageResolver) HelperImage() (string, error) {
	img := os.Getenv("RELATED_IMAGE_GITLAB_RUNNER_HELPER")
	if img == "" {
		img = "registry.gitlab.com/gitlab-org/gitlab-runner/gitlab-runner-helper:alpine-latest-x86_64-bleeding"
	}

	return img, nil
}

func (e envImageResolver) RunnerImage() (string, error) {
	img := os.Getenv("RELATED_IMAGE_GITLAB_RUNNER")
	if img == "" {
		img = "registry.gitlab.com/gitlab-org/gitlab-runner:alpine-bleeding"
	}

	return img, nil
}
