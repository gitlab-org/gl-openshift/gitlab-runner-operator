//go:build integration

package controllers

import (
	"context"
	"log"
	"os"
	"os/exec"
	"testing"
	"time"

	"github.com/go-logr/logr"
	"github.com/stretchr/testify/require"
	gitlabv1beta2 "gitlab.com/gitlab-org/gl-openshift/gitlab-runner-operator/api/v1beta2"
	"gitlab.com/gitlab-org/gl-openshift/gitlab-runner-operator/controllers/manager"
	"gotest.tools/assert"
	corev1 "k8s.io/api/core/v1"
	rbacv1 "k8s.io/api/rbac/v1"
	apiErrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

var (
	operatorNamespace = os.Getenv("INTEGRATION_TEST_OPERATOR_NAMESPACE")
	appNamespace      = os.Getenv("INTEGRATION_TEST_OPERATOR_APP_NAMESPACE")
)

func init() {
	// try setting these to a value that won't clash with other namespaces
	// but also make sure to not leave trash behind
	if operatorNamespace == "" {
		log.Fatal("INTEGRATION_TEST_OPERATOR_NAMESPACE is required")
	}
	if appNamespace == "" {
		log.Fatal("INTEGRATION_TEST_OPERATOR_APP_NAMESPACE is required")
	}
}

func setup(t *testing.T) (*manager.Manager, *RunnerReconciler, logr.Logger) {
	mgr, err := manager.New(
		manager.WithNamespace(operatorNamespace),
		manager.WithDisableCacheFor(
			&corev1.ServiceAccount{},
			&rbacv1.Role{},
			&rbacv1.RoleBinding{},
		),
	)
	require.NoError(t, err)

	rr, err := New(mgr)
	require.NoError(t, err)
	rr.reconcile = func(ctx context.Context, r *RunnerReconciler, req ctrl.Request) (ctrl.Result, error) {
		return ctrl.Result{}, nil
	}

	go mgr.Start(context.Background())

	return mgr, rr, ctrl.Log.WithName("test-logger")
}

func testServiceAccount(name, namespace string) *corev1.ServiceAccount {
	return &corev1.ServiceAccount{
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
		},
	}
}

func testRole(name, namespace string) *rbacv1.Role {
	return &rbacv1.Role{
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
		},
		Rules: []rbacv1.PolicyRule{
			{
				Verbs:     []string{"create"},
				APIGroups: []string{"test.apps.gitlab.com"},
				Resources: []string{"deployments"},
			},
		},
	}
}

func testRoleBinding(name, namespace, subjectName, subjectNamespace, roleName string) *rbacv1.RoleBinding {
	return &rbacv1.RoleBinding{
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
		},
		Subjects: []rbacv1.Subject{
			{
				Name:      subjectName,
				Namespace: subjectNamespace,
				Kind:      "ServiceAccount",
			},
		},
		RoleRef: rbacv1.RoleRef{
			Kind: "Role",
			Name: roleName,
		},
	}
}

func testNamespace(name string) *corev1.Namespace {
	return &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: name,
		},
	}
}

func deleteObjects(t *testing.T, client client.Client, objs ...client.Object) {
	for _, o := range objs {
		err := client.Delete(context.Background(), o)
		if err != nil && !apiErrors.IsNotFound(err) {
			t.Log(err)
		}
	}
}

func ensureObjects(t *testing.T, client client.Client, objs ...client.Object) func() {
	for _, o := range objs {
		err := client.Create(context.Background(), o)
		if err != nil && !apiErrors.IsAlreadyExists(err) {
			t.Log(err)
		}
	}

	return func() {
		t.Log("running cleanup")
		deleteObjects(t, client, objs...)
	}
}

func init() {
	if err := exec.Command("kubectl", "cluster-info").Run(); err != nil {
		log.Fatal("kubectl not initialized", err)
	}
}

func assertResourcesExist(t *testing.T, client client.Client, resources rbacResources, namespace string) (*corev1.ServiceAccount, *rbacv1.Role, *rbacv1.RoleBinding) {
	sa := &corev1.ServiceAccount{}
	err := client.Get(context.Background(), types.NamespacedName{
		Namespace: namespace,
		Name:      resources.serviceAccount,
	}, sa)
	require.NoError(t, err)

	role := &rbacv1.Role{}
	err = client.Get(context.Background(), types.NamespacedName{
		Namespace: namespace,
		Name:      resources.role,
	}, role)
	require.NoError(t, err)

	roleBinding := &rbacv1.RoleBinding{}
	err = client.Get(context.Background(), types.NamespacedName{
		Namespace: namespace,
		Name:      resources.roleBinding,
	}, roleBinding)
	require.NoError(t, err)

	return sa, role, roleBinding
}

func assertResourcesDoNotExist(t *testing.T, client client.Client, resources rbacResources, namespace string) {
	sa := &corev1.ServiceAccount{}
	err := client.Get(context.Background(), types.NamespacedName{
		Namespace: namespace,
		Name:      resources.serviceAccount,
	}, sa)
	require.True(t, apiErrors.IsNotFound(err))

	role := &rbacv1.Role{}
	err = client.Get(context.Background(), types.NamespacedName{
		Namespace: namespace,
		Name:      resources.role,
	}, role)
	require.True(t, apiErrors.IsNotFound(err))

	roleBinding := &rbacv1.RoleBinding{}
	err = client.Get(context.Background(), types.NamespacedName{
		Namespace: namespace,
		Name:      resources.roleBinding,
	}, roleBinding)
	require.True(t, apiErrors.IsNotFound(err))
}

func TestReconcileRBAC(t *testing.T) {
	mgr, rr, log := setup(t)

	kubeClient := mgr.GetClient()
	ensureObjects(
		t, kubeClient,
		testNamespace(operatorNamespace),
		testNamespace(appNamespace),
	)

	deleteAll := func() []client.Object {
		return []client.Object{
			testServiceAccount("gitlab-runner-sa", operatorNamespace),
			testServiceAccount("gitlab-runner-sa", appNamespace),

			testServiceAccount("gitlab-runner-app-sa", operatorNamespace),
			testServiceAccount("gitlab-runner-app-sa", appNamespace),

			testRole("gitlab-runner-role", operatorNamespace),
			testRole("gitlab-runner-role", appNamespace),
			testRole("gitlab-runner-app-role", operatorNamespace),
			testRole("gitlab-runner-app-role", appNamespace),

			testRoleBinding(
				"gitlab-runner-rolebinding",
				operatorNamespace,
				"",
				"",
				"",
			),
			testRoleBinding(
				"gitlab-runner-rolebinding",
				appNamespace,
				"",
				"",
				"",
			),
			testRoleBinding(
				"gitlab-runner-app-rolebinding",
				operatorNamespace,
				"",
				"",
				"",
			),
			testRoleBinding(
				"gitlab-runner-app-rolebinding",
				appNamespace,
				"",
				"",
				"",
			),
		}
	}

	createAll := func(rbac rbacResources) []client.Object {
		return []client.Object{
			testServiceAccount(rbac.serviceAccount, rbac.namespace),
			testRole(rbac.role, rbac.namespace),
			testRoleBinding(
				rbac.roleBinding,
				rbac.namespace,
				rbac.serviceAccount,
				"",
				rbac.role,
			),
		}
	}

	assertAllFn := func(t *testing.T, r *RunnerReconciler, namespace string) {
		_, role, roleBinding := assertResourcesExist(t, kubeClient, r.rbac, namespace)

		assert.DeepEqual(t, testRole("", "").Rules, role.Rules)

		expectedRoleBinding := testRoleBinding(
			r.rbac.roleBinding,
			operatorNamespace,
			r.rbac.serviceAccount,
			namespace,
			r.rbac.role,
		)

		assert.Equal(t, expectedRoleBinding.Subjects[0].Name, roleBinding.Subjects[0].Name)
		assert.Equal(t, expectedRoleBinding.Subjects[0].Namespace, roleBinding.Subjects[0].Namespace)
		assert.Equal(t, expectedRoleBinding.RoleRef.Name, roleBinding.RoleRef.Name)
	}

	tests := map[string]struct {
		rbac        rbacResources
		create      func(rbacResources) []client.Object
		assert      func(*testing.T, *RunnerReconciler, string)
		assertError func(*testing.T, error)
	}{
		"fresh reconcile with legacy non-app gitlab-runner-<rbac>": {
			rbac: rbacResources{
				namespace:      appNamespace,
				serviceAccount: "gitlab-runner-sa",
				role:           "gitlab-runner-role",
				roleBinding:    "gitlab-runner-rolebinding",
			},
			create: func(rbac rbacResources) []client.Object {
				return createAll(rbacResources{
					namespace:      operatorNamespace,
					serviceAccount: rbac.serviceAccount,
					role:           rbac.role,
					roleBinding:    rbac.roleBinding,
				})
			},
			assert: assertAllFn,
		},
		"fresh reconcile with gitlab-runner-app-<rbac>": {
			rbac: rbacResources{
				namespace:      appNamespace,
				serviceAccount: "gitlab-runner-app-sa",
				role:           "gitlab-runner-app-role",
				roleBinding:    "gitlab-runner-app-rolebinding",
			},
			create: func(rbac rbacResources) []client.Object {
				return createAll(rbacResources{
					namespace:      operatorNamespace,
					serviceAccount: rbac.serviceAccount,
					role:           rbac.role,
					roleBinding:    rbac.roleBinding,
				})
			},
			assert: assertAllFn,
		},
		"reconcile with both app and non-app rbac existing": {
			rbac: rbacResources{
				namespace:      appNamespace,
				serviceAccount: "gitlab-runner-sa",
				role:           "gitlab-runner-role",
				roleBinding:    "gitlab-runner-rolebinding",
			},
			create: func(rbac rbacResources) []client.Object {
				return append(
					createAll(rbacResources{
						namespace:      operatorNamespace,
						serviceAccount: rbac.serviceAccount,
						role:           rbac.role,
						roleBinding:    rbac.roleBinding,
					}),
					createAll(rbacResources{
						namespace:      operatorNamespace,
						serviceAccount: "gitlab-runner-app-sa",
						role:           "gitlab-runner-app-role",
						roleBinding:    "gitlab-runner-app-rolebinding",
					})...,
				)
			},
			assert: func(t *testing.T, r *RunnerReconciler, namespace string) {
				assertAllFn(t, r, namespace)
				assertResourcesDoNotExist(t, kubeClient, rbacResources{
					serviceAccount: "gitlab-runner-sa",
					role:           "gitlab-runner-role",
					roleBinding:    "gitlab-runner-rolebinding",
				}, namespace)
			},
		},
		"reconcile error when no rbac are available": {
			rbac: rbacResources{
				namespace:      appNamespace,
				serviceAccount: "gitlab-runner-sa",
				role:           "gitlab-runner-role",
				roleBinding:    "gitlab-runner-rolebinding",
			},
			create: func(rbac rbacResources) []client.Object {
				return []client.Object{}
			},
			assert: func(t *testing.T, rr *RunnerReconciler, namespace string) {
				assertResourcesDoNotExist(t, kubeClient, rr.rbac, namespace)
				assertResourcesDoNotExist(t, kubeClient, rbacResources{
					serviceAccount: "gitlab-runner-app-sa",
					role:           "gitlab-runner-app-role",
					roleBinding:    "gitlab-runner-app-rolebinding",
				}, namespace)
			},
			assertError: func(t *testing.T, err error) {
				require.ErrorIs(t, err, errNoRBACResourcesFound)
			},
		},
		"fresh reconcile with legacy non-app gitlab-runner-<rbac> in operator namespace": {
			rbac: rbacResources{
				namespace:      operatorNamespace,
				serviceAccount: "gitlab-runner-sa",
				role:           "gitlab-runner-role",
				roleBinding:    "gitlab-runner-rolebinding",
			},
			create: createAll,
			assert: assertAllFn,
		},
		"fresh reconcile with legacy app gitlab-runner-app-<rbac> in operator namespace": {
			rbac: rbacResources{
				namespace:      operatorNamespace,
				serviceAccount: "gitlab-runner-app-sa",
				role:           "gitlab-runner-app-role",
				roleBinding:    "gitlab-runner-app-rolebinding",
			},
			create: createAll,
			assert: assertAllFn,
		},
		"reconcile with both app and non-app rbac existing in operator namespace": {
			rbac: rbacResources{
				namespace:      operatorNamespace,
				serviceAccount: "gitlab-runner-sa",
				role:           "gitlab-runner-role",
				roleBinding:    "gitlab-runner-rolebinding",
			},
			create: func(rbac rbacResources) []client.Object {
				return append(
					createAll(rbac),
					createAll(rbacResources{
						namespace:      operatorNamespace,
						serviceAccount: "gitlab-runner-app-sa",
						role:           "gitlab-runner-app-role",
						roleBinding:    "gitlab-runner-app-rolebinding",
					})...,
				)
			},
			assert: func(t *testing.T, r *RunnerReconciler, namespace string) {
				assertAllFn(t, r, namespace)
			},
		},
		"reconcile when legacy rbac exist in app namespace and non-legacy rbac exists in operator namespace": {
			rbac: rbacResources{
				namespace:      appNamespace,
				serviceAccount: "gitlab-runner-sa",
				role:           "gitlab-runner-role",
				roleBinding:    "gitlab-runner-rolebinding",
			},
			create: func(rbac rbacResources) []client.Object {
				return append(
					createAll(rbacResources{
						namespace:      operatorNamespace,
						serviceAccount: "gitlab-runner-app-sa",
						role:           "gitlab-runner-app-role",
						roleBinding:    "gitlab-runner-app-rolebinding",
					}),
					createAll(rbac)...,
				)
			},
			assert: func(t *testing.T, reconciler *RunnerReconciler, namespace string) {
				assertResourcesExist(t, kubeClient, reconciler.rbac, namespace)
				assertResourcesDoNotExist(t, kubeClient, rbacResources{
					serviceAccount: "gitlab-runner-app-sa",
					role:           "gitlab-runner-app-role",
					roleBinding:    "gitlab-runner-app-rolebinding",
				}, namespace)
			},
		},
	}

	originalGetOperatorNamespace := getOperatorNamespace
	getOperatorNamespace = func(_ string) string {
		return operatorNamespace
	}
	defer func() {
		getOperatorNamespace = originalGetOperatorNamespace
	}()

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			deleteObjects(t, kubeClient, deleteAll()...)
			cleanup := ensureObjects(t, kubeClient, tt.create(tt.rbac)...)
			defer cleanup()

			err := rr.reconcileRBAC(context.Background(), log, &gitlabv1beta2.Runner{
				ObjectMeta: metav1.ObjectMeta{
					Namespace: tt.rbac.namespace,
				},
			})
			if tt.assertError == nil {
				require.NoError(t, err)
			} else {
				tt.assertError(t, err)
			}

			time.Sleep(time.Second) // wait for the k8s api

			tt.assert(t, rr, tt.rbac.namespace)
		})
	}

}
