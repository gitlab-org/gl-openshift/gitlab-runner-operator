package manager

import (
	"fmt"
	"os"

	appsv1beta2 "gitlab.com/gitlab-org/gl-openshift/gitlab-runner-operator/api/v1beta2"
	"k8s.io/apimachinery/pkg/runtime"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

const (
	webhookCertDir  = "/apiserver.local.config/certificates"
	webhookCertName = "apiserver.crt"
	webhookKeyName  = "apiserver.key"
)

func isOLMCertPresent() bool {
	_, err := os.Stat(webhookCertDir)
	return err == nil
}

var scheme = runtime.NewScheme()

func init() {
	utilruntime.Must(clientgoscheme.AddToScheme(scheme))

	utilruntime.Must(appsv1beta2.AddToScheme(scheme))
	// +kubebuilder:scaffold:scheme
}

// Option is a manager configuration option
type Option func(*ctrl.Options)

// WithMetricsAddr sets the address the controller-runtime's metrics server binds to
func WithMetricsAddr(addr string) Option {
	return func(options *ctrl.Options) {
		options.MetricsBindAddress = addr
	}
}

// WithLeaderElection enables leader election for the manager
func WithLeaderElection(enableLeaderElection bool) Option {
	return func(options *ctrl.Options) {
		options.LeaderElection = enableLeaderElection
	}
}

// WithNamespace sets the namespace the manager watches
func WithNamespace(namespace string) Option {
	return func(options *ctrl.Options) {
		options.Namespace = namespace
	}
}

// WithDisableCacheFor disables the cache for the given objects
func WithDisableCacheFor(objs ...client.Object) Option {
	return func(options *ctrl.Options) {
		options.ClientDisableCacheFor = append(options.ClientDisableCacheFor, objs...)
	}
}

// Manager is a wrapper for controller-runtime ctrl.Manager and ctrl.Options
type Manager struct {
	ctrl.Manager

	Opts ctrl.Options
}

// New creates a new controller-runtime manager wrapper
func New(withOpts ...Option) (*Manager, error) {
	opts := ctrl.Options{
		Scheme:           scheme,
		Port:             9443,
		LeaderElectionID: "b43e192d.gitlab.com",
		Namespace:        getWatchedNamespace(),
	}

	for _, o := range withOpts {
		o(&opts)
	}

	mgr, err := ctrl.NewManager(ctrl.GetConfigOrDie(), opts)
	if err != nil {
		return nil, fmt.Errorf("unable to start manager %w", err)
	}

	// fix to use the OLM provided Certificate
	if isOLMCertPresent() {
		mgr.GetWebhookServer().CertDir = webhookCertDir
		mgr.GetWebhookServer().CertName = webhookCertName
		mgr.GetWebhookServer().KeyName = webhookKeyName
	}

	// +kubebuilder:scaffold:builder
	return &Manager{
		Manager: mgr,
		Opts:    opts,
	}, nil
}

func getWatchedNamespace() string {
	return os.Getenv("WATCH_NAMESPACES")
}
