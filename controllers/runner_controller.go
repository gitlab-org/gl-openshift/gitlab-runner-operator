/*


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"errors"
	"fmt"
	"os"
	"reflect"
	"sort"
	"strings"
	"sync"
	"time"

	"github.com/go-logr/logr"
	"github.com/google/uuid"
	gitlabv1beta2 "gitlab.com/gitlab-org/gl-openshift/gitlab-runner-operator/api/v1beta2"
	runnerctl "gitlab.com/gitlab-org/gl-openshift/gitlab-runner-operator/controllers/runner"
	gitlabutils "gitlab.com/gitlab-org/gl-openshift/gitlab-runner-operator/controllers/utils"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	rbacv1 "k8s.io/api/rbac/v1"
	apiErrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/util/workqueue"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/event"
	"sigs.k8s.io/controller-runtime/pkg/predicate"
)

const (
	cleanupFinalizer string = "finalizer.gitlab.com"
)

var (
	errTokenNotFound        = errors.New("token secret not found")
	errNoRBACResourcesFound = errors.New("no RBAC resources were found for selection, at least one group should exist")
)

type rbacResources struct {
	namespace      string
	serviceAccount string
	role           string
	roleBinding    string
}

// supportedRoleBindings is the list of supported grouped auth resources
// "gitlab-runner-rolebinding" and the related resources are legacy resources
// they've been renamed to more accurately show that these aren't used
// by the GitLab Runner Operator but by the GitLab Runner application to
// "gitlab-runner-app-rolebinding"
// when resolving these resources for reconciliation the rolebinding is picked first
// and the role and service account are resolved from its subjects and roleRef
// the gitlab-runner-app-rolebinding is given priority over the gitlab-runner-rolebinding
// but in cases where and upgrade is happening and the gitlab-runner-app-rolebinding is not found
// the gitlab-runner-rolebinding is used
var supportedRoleBindings = []string{"gitlab-runner-app-rolebinding", "gitlab-runner-rolebinding"}

// RunnerReconciler reconciles a Runner object
type RunnerReconciler struct {
	Client

	rbacMutex  sync.Mutex
	rbac       rbacResources
	sourceRbac rbacResources

	// a function to easily mock in integration tests
	reconcile reconcileFunc

	// DEPRECATED: Should be removed when the `registration-token` is deprecated
	IsAuthToken bool
}

// Compile-time check that we fulfil the Client interface
// This will guarantee that our mocks and interfaces are up-to-date with the
// controller-runtime library.
var _ client.Client = &RunnerReconciler{}

// New creates a new RunnerReconciler from a ctrl.Manager
func New(mgr ctrl.Manager) (*RunnerReconciler, error) {
	rr := &RunnerReconciler{
		Client:    NewRunnerReconcilerClient(mgr.GetClient()),
		reconcile: defaultReconcile,
	}

	if err := rr.setupWithManager(mgr); err != nil {
		return nil, err
	}

	return rr, nil
}

// setupWithManager configures the custom resource watched resources
func (r *RunnerReconciler) setupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&gitlabv1beta2.Runner{}).
		Owns(&corev1.Secret{}).
		Owns(&corev1.Service{}).
		Owns(&corev1.ConfigMap{}).
		Owns(&appsv1.Deployment{}).
		WithEventFilter(runnerEventsFilter()).
		WithOptions(controller.Options{
			RateLimiter: workqueue.NewMaxOfRateLimiter(
				workqueue.NewItemFastSlowRateLimiter(time.Second, 10*time.Second, 3),
			),
		}).
		Complete(r)
}

func runnerEventsFilter() predicate.Predicate {
	return predicate.Funcs{
		DeleteFunc: func(event.DeleteEvent) bool {
			return true
		},
	}
}

type reconcileFunc func(ctx context.Context, r *RunnerReconciler, req ctrl.Request) (ctrl.Result, error)

func defaultReconcile(ctx context.Context, r *RunnerReconciler, req ctrl.Request) (ctrl.Result, error) {
	log := ctrl.Log.
		WithName("controllers").
		WithName("Runner").
		WithValues("runner", req.NamespacedName, "id", uuid.New())

	log.Info("Reconciling Runner")

	runner := &gitlabv1beta2.Runner{}
	if err := r.Get(ctx, req.NamespacedName, runner); err != nil {
		if apiErrors.IsNotFound(err) {
			return ctrl.Result{}, nil
		}

		return ctrl.Result{}, err
	}

	// setup finalizers for garbage collection
	if err := r.setupRunnerFinalizer(ctx, runner); err != nil {
		r.updateStatusMessage(ctx, runner, err)
		return ctrl.Result{}, err
	}

	if runner.DeletionTimestamp != nil {
		return ctrl.Result{}, nil
	}

	if err := r.validateRegistrationTokenSecret(ctx, log, runner); err != nil {
		r.updateStatusMessage(ctx, runner, err)
		if errors.Is(err, errTokenNotFound) {
			return ctrl.Result{RequeueAfter: time.Second * 10}, nil
		}
		return ctrl.Result{}, err
	}

	if err := r.reconcileRBAC(ctx, log, runner); err != nil {
		r.updateStatusMessage(ctx, runner, err)
		return ctrl.Result{}, err
	}

	if err := r.reconcileConfigMaps(ctx, runner); err != nil {
		r.updateStatusMessage(ctx, runner, err)
		return ctrl.Result{}, err
	}

	if err := r.reconcileDeployments(ctx, runner, log); err != nil {
		r.updateStatusMessage(ctx, runner, err)
		return ctrl.Result{}, err
	}

	if err := r.reconcileStatus(ctx, runner); err != nil {
		r.updateStatusMessage(ctx, runner, err)
		return ctrl.Result{}, err
	}

	r.updateStatusMessage(ctx, runner, nil)

	return ctrl.Result{}, nil
}

// +kubebuilder:rbac:groups=apps.gitlab.com,resources=runners,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=apps.gitlab.com,resources=runners/finalizers,verbs=update;patch;delete
// +kubebuilder:rbac:groups=apps.gitlab.com,resources=runners/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=apps,resources=deployments,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=serviceaccounts,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=services,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=secrets,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=configmaps,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=events,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=pods,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=rbac.authorization.k8s.io,resources=roles,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=rbac.authorization.k8s.io,resources=rolebindings,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=pods/log,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=pods/exec,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=pods/attach,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=persistentvolumeclaims,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=resourcequotas,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=services/finalizers,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=services/proxy,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=services/status,verbs=get;list;watch;create;update;patch;delete

// Reconcile triggers when an event occurs on the watched resource
func (r *RunnerReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	return r.reconcile(ctx, r, req)
}

func (r *RunnerReconciler) setupRunnerFinalizer(ctx context.Context, cr *gitlabv1beta2.Runner) error {
	origFinalizers := cr.Finalizers

	if cr.DeletionTimestamp == nil && len(cr.Finalizers) == 0 {
		cr.Finalizers = []string{
			cleanupFinalizer,
		}
	}

	if cr.DeletionTimestamp != nil && len(cr.Finalizers) == 1 {
		// clean up shared resources if there are no other
		// GitLab Runner instances in the project
		if err := r.projectCleanup(ctx, cr); err != nil {
			if apiErrors.IsNotFound(err) {
				return nil
			}
			return err
		}

		cr.Finalizers = []string{}
	}

	if !reflect.DeepEqual(origFinalizers, cr.Finalizers) {
		if err := r.Update(ctx, cr); err != nil {
			if apiErrors.IsNotFound(err) {
				return nil
			}
			return err
		}
	}

	return nil
}

func (r *RunnerReconciler) reconcileConfigMaps(ctx context.Context, cr *gitlabv1beta2.Runner) error {
	configs := runnerctl.ConfigMap(cr)

	// check user provided config.toml
	if cr.Spec.Configuration != "" {
		if err := r.getUserConfigToml(ctx, cr); err != nil {
			return err
		}
	}

	found := &corev1.ConfigMap{}
	err := r.Get(ctx, types.NamespacedName{Name: configs.Name, Namespace: cr.Namespace}, found)
	if err != nil {
		if apiErrors.IsNotFound(err) {
			if err := controllerutil.SetControllerReference(cr, configs, r.Scheme()); err != nil {
				return err
			}

			return r.Create(ctx, configs)
		}

		return err
	}

	if cm, changed := gitlabutils.IsConfigMapChanged(found, configs); changed {
		return r.Update(ctx, cm)
	}

	return nil
}

// The getUserConfigToml retrieves the user provided config.tom and merges it with the
// GitLab Runner generated configuration as a configuration template
// https://docs.gitlab.com/runner/register/#runners-configuration-template-file
func (r *RunnerReconciler) getUserConfigToml(ctx context.Context, cr *gitlabv1beta2.Runner) error {
	userCM := &corev1.ConfigMap{}
	userCMKey := types.NamespacedName{
		Name:      cr.Spec.Configuration,
		Namespace: cr.Namespace,
	}

	if err := r.Get(ctx, userCMKey, userCM); err != nil {
		return err
	}

	if _, ok := userCM.Data["config.toml"]; !ok {
		return fmt.Errorf("config.toml not found")
	}

	return nil
}

func (r *RunnerReconciler) reconcileDeployments(ctx context.Context, cr *gitlabv1beta2.Runner, log logr.Logger) error {
	envs := r.parseCustomEnvironments(ctx, cr)
	ctl := runnerctl.New(cr, r.IsAuthToken)

	r.rbacMutex.Lock()
	// the rbac resources have been resolved by this point
	serviceAccount := r.rbac.serviceAccount
	r.rbacMutex.Unlock()

	runner, err := ctl.Deployment(envs, serviceAccount)
	if err != nil {
		return err
	}

	if err := r.appendConfigMapChecksum(ctx, runner); err != nil {
		log.Error(err, "Error appending configmap checksums")
	}

	found := &appsv1.Deployment{}
	err = r.Get(ctx, types.NamespacedName{Name: runner.Name, Namespace: cr.Namespace}, found)
	if err != nil {
		if apiErrors.IsNotFound(err) {
			if err := controllerutil.SetControllerReference(cr, runner, r.Scheme()); err != nil {
				return err
			}

			return r.Create(ctx, runner)
		}

		return err
	}

	deployment, changed := gitlabutils.IsDeploymentChanged(found, runner)
	if changed {
		return r.Update(ctx, deployment)
	}

	return nil
}

func (r *RunnerReconciler) reconcileMetrics(ctx context.Context, cr *gitlabv1beta2.Runner) error {
	svc := runnerctl.MetricsService(cr)

	found := &corev1.Service{}
	err := r.Get(ctx, types.NamespacedName{Name: svc.Name, Namespace: cr.Namespace}, found)
	if err != nil {
		if apiErrors.IsNotFound(err) {
			if err := controllerutil.SetControllerReference(cr, svc, r.Scheme()); err != nil {
				return err
			}

			return r.Create(ctx, svc)
		}

		return err
	}

	if !reflect.DeepEqual(svc.Spec, found.Spec) {
		// besides ClusterIP, not much is expected to change
		// return r.Update(ctx, found)
		return nil
	}

	return nil
}

// func (r *RunnerReconciler) reconcileServiceMonitor(ctx context.Context, cr *gitlabv1beta2.Runner) error {

// 	if gitlabutils.IsPrometheusSupported() {
// 		sm := runnerctl.ServiceMonitorService(cr)

// 		found := &monitoringv1.ServiceMonitor{}
// 		err := r.Get(ctx, types.NamespacedName{Name: sm.Name, Namespace: cr.Namespace}, found)
// 		if err != nil {
// 			if apiErrors.IsNotFound(err) {
// 				if err := controllerutil.SetControllerReference(cr, sm, r.Scheme); err != nil {
// 					return err
// 				}

// 				return r.Create(ctx, sm)
// 			}

// 			return err
// 		}

// 		if !reflect.DeepEqual(sm.Spec, found.Spec) {
// 			found.Spec = sm.Spec
// 			return r.Update(ctx, found)
// 		}
// 	}

// 	return nil
// }

func (r *RunnerReconciler) validateRegistrationTokenSecret(ctx context.Context, log logr.Logger, cr *gitlabv1beta2.Runner) error {
	// check if the token secret exists
	tokenSecret := &corev1.Secret{}
	tokenKey := runnerctl.RegistrationTokenSecret(cr)
	if err := r.Get(ctx, tokenKey, tokenSecret); err != nil {
		if apiErrors.IsNotFound(err) {
			log.Error(err, "token secret not found", "name", tokenKey.Name)

			if cr.Status.Phase != RunnerWaiting {
				cr.Status.Phase = RunnerWaiting
				if err := r.updateRunnerStatus(ctx, cr); err != nil {
					return err
				}
			}

			return errTokenNotFound
		}

		return nil
	}

	registrationToken := tokenSecret.Data["runner-registration-token"]
	runnerToken := tokenSecret.Data["runner-token"]

	regTokenStr := strings.TrimSpace(string(registrationToken))
	runnerTokenStr := strings.TrimSpace(string(runnerToken))

	if regTokenStr == "" && runnerTokenStr == "" {
		return fmt.Errorf("runner-registration-token or runner-token are needed")
	}

	if regTokenStr != "" && runnerTokenStr != "" {
		return fmt.Errorf("both runner-registration-token and runner-token were supplied, please supply a single token")
	}

	// DEPRECATED: Should be removed when the `registration-token` is deprecated
	r.IsAuthToken = strings.HasPrefix(regTokenStr, "glrt-") || strings.HasPrefix(runnerTokenStr, "glrt-")
	if regTokenStr != "" && !r.IsAuthToken {
		log.Info("legacy registration token is used. Their use will be removed starting 18.0")
	}

	// In the previous implementation, we were making sure that both keys (runner-registration-token and runner-token)
	// were set/available in the tokenSecret. This is why we also set the two keys even though only one is expected
	// https://gitlab.com/gitlab-org/gl-openshift/gitlab-runner-operator/-/blob/51b12ec47dcd4e5c358339e9c230ffde8b568daa/controllers/runner_controller.go#L376-379
	if regTokenStr != tokenSecret.StringData["runner-registration-token"] || runnerTokenStr != tokenSecret.StringData["runner-token"] {
		tokenSecret.Data["runner-registration-token"] = registrationToken
		tokenSecret.Data["runner-token"] = runnerToken

		return r.Update(ctx, tokenSecret)
	}

	return nil
}

func (r *RunnerReconciler) appendConfigMapChecksum(ctx context.Context, deployment *appsv1.Deployment) error {
	configmaps := gitlabutils.DeploymentConfigMaps(deployment)

	for _, cmName := range configmaps {
		found := &corev1.ConfigMap{}
		err := r.Get(ctx, types.NamespacedName{Name: cmName, Namespace: deployment.Namespace}, found)
		if err != nil {
			return err
		}

		// get checksum from the configmap annotation
		if checksum, ok := found.Annotations["checksum"]; ok {
			// compare the checksum with cm checksum in deployment template annotation
			if val, ok := deployment.Spec.Template.Annotations[cmName]; ok {
				if val != checksum {
					deployment.Spec.Template.Annotations[cmName] = checksum
				}
			} else {
				// account for nil map exception
				if deployment.Spec.Template.Annotations != nil {
					deployment.Spec.Template.Annotations[cmName] = checksum
				} else {
					deployment.Spec.Template.Annotations = map[string]string{
						cmName: checksum,
					}
				}
			}
		}
	}

	return nil
}

var getOperatorNamespace = func(fallback string) string {
	// The operator namespace can be obtained from the environment variable OPERATOR_NAMESPACE,
	// which is populated by the olm.operatorNamespace annotation added by OLM,
	// or by retrieving the namespace where the manager Pod is running.
	if namespace := os.Getenv("OPERATOR_NAMESPACE"); namespace != "" {
		return namespace
	}

	return fallback
}

func (r *RunnerReconciler) reconcileRBAC(ctx context.Context, log logr.Logger, cr *gitlabv1beta2.Runner) error {
	rbac, err := r.selectRBACResources(ctx, log, cr.Namespace)
	if err != nil {
		return err
	}

	sourceRbac, err := r.selectRBACResources(ctx, log, getOperatorNamespace(cr.Namespace))
	if err != nil {
		return err
	}
	r.rbacMutex.Lock()
	// use the rbac variable here to make sure the rbac resources are consistent across different reconcile calls
	// it's an edge-case but still worth to avoid confusion when syncing resources in the rare case where
	// one switches from one resource to another
	// also avoids the needs to synchronize more reads
	// we need these values stored in the reconciler for the cleanup methods for best-effort cleanup
	r.rbac = rbac
	r.sourceRbac = sourceRbac
	r.rbacMutex.Unlock()

	if err := r.reconcileServiceAccount(ctx, log, rbac); err != nil {
		return err
	}

	if err := r.reconcileRunnerRole(ctx, log, rbac, sourceRbac); err != nil {
		return err
	}

	return r.reconcileRunnerRoleBinding(ctx, log, rbac, sourceRbac)
}

func (r *RunnerReconciler) selectRBACResources(ctx context.Context, log logr.Logger, namespace string) (rbacResources, error) {
	operatorNamespace := getOperatorNamespace(namespace)

	// If the operator namespace and the namespace where the runner is deployed are different
	// we need to check first the app namespace for any existing role bindings and roles
	namespaces := []string{namespace}
	if operatorNamespace != namespace {
		namespaces = append(namespaces, operatorNamespace)
	}

	for _, ns := range namespaces {
		for _, rolebindingName := range supportedRoleBindings {
			// Both role and rolebinding should exist
			log.Info(fmt.Sprintf("checking for selection RBAC roleBinding %q", rolebindingName))
			var rb rbacv1.RoleBinding
			if err := r.Get(ctx, types.NamespacedName{
				Name:      rolebindingName,
				Namespace: ns,
			}, &rb); apiErrors.IsNotFound(err) {
				continue
			}

			log.Info(fmt.Sprintf("checking for selection RBAC role %q", rb.RoleRef.Name))
			var role rbacv1.Role
			if err := r.Get(ctx, types.NamespacedName{
				Name:      rb.RoleRef.Name,
				Namespace: ns,
			}, &role); apiErrors.IsNotFound(err) {
				continue
			}

			var serviceAccount string
			for _, subj := range rb.Subjects {
				if subj.Kind == "ServiceAccount" {
					serviceAccount = subj.Name
					break
				}
			}
			if serviceAccount == "" {
				return rbacResources{}, fmt.Errorf("no service account found in role binding %v", rb)
			}

			return rbacResources{
				serviceAccount: serviceAccount,
				role:           role.Name,
				roleBinding:    rb.Name,
				namespace:      namespace,
			}, nil
		}
	}

	return rbacResources{}, errNoRBACResourcesFound
}

func (r *RunnerReconciler) reconcileServiceAccount(ctx context.Context, log logr.Logger, rbac rbacResources) error {
	sa := newServiceAccount(rbac.serviceAccount, rbac.namespace)
	lookupKey := types.NamespacedName{
		Name:      sa.Name,
		Namespace: sa.Namespace,
	}

	log = log.WithValues("serviceAccount", sa.Name, "namespace", sa.Namespace)

	found := &corev1.ServiceAccount{}
	if err := r.Get(ctx, lookupKey, found); err != nil {
		if apiErrors.IsNotFound(err) {
			log.Info("Creating service account")
			return r.Create(ctx, sa)
		}

		return err
	}

	log.Info("Found service account")
	return nil
}

func newObjectMeta(name, namespace string) metav1.ObjectMeta {
	meta := metav1.ObjectMeta{
		Name: name,
		Labels: map[string]string{
			"app.kubernetes.io/name":       name,
			"app.kubernetes.io/created-by": "gitlab-runner-operator",
		},
	}

	if namespace != "" {
		meta.Namespace = namespace
	}

	return meta
}

// ServiceAccount returns service account to be used by pods
func newServiceAccount(name, namespace string) *corev1.ServiceAccount {
	return &corev1.ServiceAccount{
		TypeMeta: metav1.TypeMeta{
			Kind: "ServiceAccount",
		},
		ObjectMeta: newObjectMeta(name, namespace),
	}
}

func newRole(name, namespace string) *rbacv1.Role {
	return &rbacv1.Role{
		TypeMeta: metav1.TypeMeta{
			Kind: "Role",
		},
		ObjectMeta: newObjectMeta(name, namespace),
	}
}

func newRoleBinding(name, namespace string) *rbacv1.RoleBinding {
	return &rbacv1.RoleBinding{
		TypeMeta: metav1.TypeMeta{
			Kind: "RoleBinding",
		},
		ObjectMeta: newObjectMeta(name, namespace),
	}
}

// Reconcile gitlab-runner-app-role with the role from the operator namespace
func (r *RunnerReconciler) reconcileRunnerRole(ctx context.Context, log logr.Logger, rbac, sourceRbac rbacResources) error {
	role := newRole(sourceRbac.role, sourceRbac.namespace)
	found := newRole(rbac.role, rbac.namespace)

	if err := ensureRBACResource(ctx, r.Client, log, role, found); err != nil {
		return err
	}

	if !reflect.DeepEqual(role.Rules, found.Rules) {
		found.Rules = role.Rules
		return r.Update(ctx, found)
	}

	return nil
}

// Reconcile gitlab-runner-app-rolebinding with the role binding from the operator namespace
func (r *RunnerReconciler) reconcileRunnerRoleBinding(ctx context.Context, log logr.Logger, rbac, sourceRbac rbacResources) error {
	roleBinding := newRoleBinding(sourceRbac.roleBinding, sourceRbac.namespace)
	found := newRoleBinding(rbac.roleBinding, rbac.namespace)

	if err := ensureRBACResource(ctx, r.Client, log, roleBinding, found); err != nil {
		return err
	}

	serviceAccountSubjectIndex := -1
	for i, subj := range found.Subjects {
		if subj.Kind == "ServiceAccount" {
			serviceAccountSubjectIndex = i
			break
		}
	}

	subjectNamespaceIsEqual := found.Subjects[serviceAccountSubjectIndex].Namespace == rbac.namespace
	// we can only update the rolebinding's subjects, not the roleref
	if serviceAccountSubjectIndex == -1 || !subjectNamespaceIsEqual {
		if len(found.Subjects) == 0 {
			found.Subjects = []rbacv1.Subject{
				{
					Kind: "ServiceAccount",
				},
			}
		}

		for i, subj := range found.Subjects {
			if subj.Kind == "ServiceAccount" {
				found.Subjects[i].Namespace = rbac.namespace
				break
			}
		}

		return r.Update(ctx, found)
	}

	return nil
}

func ensureRBACResource[T client.Object](
	ctx context.Context,
	client Client,
	log logr.Logger,
	sourceObj, destObj T,
) error {
	kind := sourceObj.GetObjectKind().GroupVersionKind().Kind

	objKey := types.NamespacedName{Namespace: destObj.GetNamespace(), Name: destObj.GetName()}
	operatorKey := types.NamespacedName{Namespace: sourceObj.GetNamespace(), Name: sourceObj.GetName()}

	log = log.WithValues("resourceType", kind, "objKey", objKey, "operatorKey", operatorKey)
	log.Info("Reconciling")

	if err := client.Get(ctx, operatorKey, sourceObj); err != nil {
		return err
	}

	log.WithValues(kind, sourceObj).Info("Found source")

	err := client.Get(ctx, objKey, destObj)
	if err == nil {
		log.Info("Found destination")
		return nil
	}

	if apiErrors.IsNotFound(err) {
		sourceObj.SetResourceVersion("")
		sourceObj.SetNamespace(destObj.GetNamespace())
		sourceObj.SetName(destObj.GetName())

		log.WithValues("obj", sourceObj).Info("Not found, creating...")
		err := client.Create(ctx, sourceObj)
		if err != nil && !apiErrors.IsAlreadyExists(err) {
			return err
		}

		return client.Get(ctx, objKey, destObj)
	}

	return err

}

func (r *RunnerReconciler) parseCustomEnvironments(ctx context.Context, cr *gitlabv1beta2.Runner) []corev1.EnvVar {
	if cr.Spec.Environment == "" {
		return []corev1.EnvVar{}
	}

	envConfigMapKey := types.NamespacedName{
		Name:      cr.Spec.Environment,
		Namespace: cr.Namespace,
	}

	envConfigMap := &corev1.ConfigMap{}
	if err := r.Get(ctx, envConfigMapKey, envConfigMap); err != nil {
		return []corev1.EnvVar{}
	}

	// extract env keys into separate slice and sort them, map iteration order is random
	// see https://nathanleclaire.com/blog/2014/04/27/a-surprising-feature-of-golang-that-colored-me-impressed/
	var keys []string
	for key := range envConfigMap.Data {
		keys = append(keys, key)
	}
	sort.Strings(keys)

	environments := make([]corev1.EnvVar, 0, len(keys))
	for _, key := range keys {
		environments = append(
			environments,
			corev1.EnvVar{
				Name:  key,
				Value: envConfigMap.Data[key],
			},
		)
	}

	return environments
}

func (r *RunnerReconciler) projectCleanup(ctx context.Context, cr *gitlabv1beta2.Runner) error {
	runners := &gitlabv1beta2.RunnerList{}

	if err := r.List(ctx, runners, client.InNamespace(cr.Namespace)); err != nil {
		return err
	}

	operatorNS := getOperatorNamespace(cr.Namespace)
	if len(runners.Items) == 1 && cr.Namespace != operatorNS {
		// delete "gitlab-runner-app-sa" service account
		if err := r.cleanUpServiceAccount(ctx, cr.Namespace); err != nil {
			return err
		}

		// delete "gitlab-runner-app-role" role
		if err := r.cleanUpRole(ctx, cr.Namespace); err != nil {
			return err
		}

		// remove "gitlab-runner-app-rolebinding" role binding
		if err := r.cleanUpRoleBinding(ctx, cr.Namespace); err != nil {
			return err
		}
	}

	return nil
}

func (r *RunnerReconciler) cleanUpServiceAccount(ctx context.Context, namespace string) error {
	r.rbacMutex.Lock()
	serviceAccountName := r.rbac.serviceAccount
	r.rbacMutex.Unlock()

	saKey := types.NamespacedName{
		Name:      serviceAccountName,
		Namespace: namespace,
	}
	sa := newServiceAccount(saKey.Name, saKey.Namespace)
	if err := r.Delete(ctx, sa); err != nil {
		if apiErrors.IsNotFound(err) {
			return nil
		}
		return err
	}

	return nil
}

func (r *RunnerReconciler) cleanUpRole(ctx context.Context, namespace string) error {
	r.rbacMutex.Lock()
	roleName := r.rbac.role
	r.rbacMutex.Unlock()

	roleKey := types.NamespacedName{
		Name:      roleName,
		Namespace: namespace,
	}
	role := &rbacv1.Role{}
	if err := r.Get(ctx, roleKey, role); err != nil {
		if apiErrors.IsNotFound(err) {
			return nil
		}
		return err
	}

	if err := r.Delete(ctx, role); err != nil {
		if apiErrors.IsNotFound(err) {
			return nil
		}
		return err
	}

	return nil
}

func (r *RunnerReconciler) cleanUpRoleBinding(ctx context.Context, namespace string) error {
	r.rbacMutex.Lock()
	roleBindingName := r.rbac.roleBinding
	r.rbacMutex.Unlock()

	rbKey := types.NamespacedName{
		Name:      roleBindingName,
		Namespace: namespace,
	}
	rb := &rbacv1.RoleBinding{}
	if err := r.Get(ctx, rbKey, rb); err != nil {
		if apiErrors.IsNotFound(err) {
			return nil
		}
		return err
	}

	if err := r.Delete(ctx, rb); err != nil {
		if apiErrors.IsNotFound(err) {
			return nil
		}
		return err
	}

	return nil
}
