package main

import (
	"fmt"
	"slices"
	"strings"

	"github.com/magefile/mage/mg"
	"github.com/magefile/mage/sh"
	"github.com/samber/lo"
	"golang.org/x/mod/semver"
)

const defaultVersion = "v0.0.1"

// Versions defines the set of commands used for version management.
type Versions mg.Namespace

// Previous prints the previous version from the list of git tags
func (Versions) Previous(current string) error {
	err := sh.RunV("git", "fetch", "--tags")
	if err != nil {
		return err
	}

	tags, err := sh.Output("git", "tag", "--list")
	if err != nil {
		return err
	}

	versions := lo.FilterMap(strings.Split(tags, "\n"), func(t string, _ int) (string, bool) {
		return strings.TrimSpace(t), t != "" && !strings.Contains(t, "rc")
	})

	slices.SortFunc(versions, func(v, w string) int {
		return -semver.Compare(v, w)
	})

	ver := defaultVersion

	for i, v := range versions {
		if semver.Compare(v, current) == 0 && len(versions) >= i+1 {
			ver = versions[i+1]
			break
		}
	}

	if ver == defaultVersion {
		if current != "" {
			ver = versions[0]
		} else if len(versions) > 1 {
			ver = versions[1]
		} else if len(versions) == 1 {
			ver = versions[0]
		}
	}

	fmt.Println(ver)
	return nil
}
