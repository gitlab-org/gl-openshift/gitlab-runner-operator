package main

import (
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"text/template"
	"time"

	"github.com/magefile/mage/mg"
	"github.com/samber/lo"
)

// Template renders project templates
type Template mg.Namespace

// ConfigManifest finds all files in the config directory which end with .tpl.yaml, including subdirectories
// and parses them as templates, writing the output to the same directory without the .tpl suffix. The `isK8s` variable is injected into the template as `false`.
func (Template) ConfigManifest(target string, vars string) error {
	supportedTargets := []string{"kubernetes", "kubernetes-olm", "openshift"}
	if !lo.Contains(supportedTargets, target) {
		return fmt.Errorf("unsupported target %s, supported targets are %s", target, strings.Join(supportedTargets, ", "))
	}

	return configManifest(target, parseVars(vars))
}

func parseVars(vars string) map[string]any {
	parsers := []func(string) (any, error){
		func(s string) (any, error) {
			return strconv.ParseBool(s)
		},
		func(s string) (any, error) {
			return strconv.ParseInt(s, 10, 64)
		},
		func(s string) (any, error) {
			return strconv.ParseFloat(s, 64)
		},
		func(s string) (any, error) {
			return s, nil
		},
	}

	res := make(map[string]any)
	for _, kv := range strings.Split(vars, " ") {
		k, v := strings.Split(kv, "=")[0], strings.Split(kv, "=")[1]
		for _, p := range parsers {
			if parsed, err := p(strings.TrimSpace(v)); err == nil {
				res[strings.TrimSpace(k)] = parsed
				break
			}
		}
	}

	return res
}

// configManifest finds all files in the config directory which end with .tpl.yaml, including subdirectories
// and parses them as templates, writing the output to the same directory without the .tpl suffix.
func configManifest(target string, vars map[string]any) error {
	var files []string
	err := filepath.WalkDir("config", func(path string, d os.DirEntry, err error) error {
		if err != nil {
			return err
		}

		if d.IsDir() {
			return nil
		}

		if strings.HasSuffix(path, ".tpl.yaml") {
			files = append(files, path)
		}

		return nil
	})
	if err != nil {
		return err
	}

	context := map[string]any{
		"Vars":    vars,
		"Target":  target,
		"TimeNow": time.Now().Format(time.RFC3339),
	}

	for _, file := range files {
		if err := writeTemplateFile(file, context); err != nil {
			return err
		}
	}

	return nil
}

func writeTemplateFile(file string, context map[string]any) error {
	b, err := os.ReadFile(file)
	if err != nil {
		return err
	}

	// parse the template
	tpl, err := template.New(filepath.Base(file)).Parse(string(b))
	if err != nil {
		return err
	}

	outFile := fmt.Sprintf("%s.yaml", strings.TrimSuffix(file, ".tpl.yaml"))
	f, err := os.Create(outFile)
	if err != nil {
		return err
	}
	defer f.Close()

	if err := tpl.Execute(f, context); err != nil {
		return err
	}

	fmt.Printf("Generated %s\n", outFile)

	return nil
}
