package main

import (
	"crypto/sha1"
	"encoding/hex"
	"fmt"
	"regexp"
	"strings"
	"time"

	"github.com/magefile/mage/mg"
	"github.com/magefile/mage/sh"
)

type Docker mg.Namespace

//go:generate mockery --name=dockerInterface --inpackage
type dockerInterface interface {
	Push(image string) error
	Inspect(image string) error
}

type dockerCLI struct{}

func (dockerCLI) Push(image string) error {
	return sh.RunV("docker", "push", image)
}

func (dockerCLI) Inspect(image string) error {
	return sh.RunV("docker", "manifest", "inspect", image)
}

var newDocker = func() dockerInterface {
	return dockerCLI{}
}

var newHash = func() string {
	now := []byte(time.Now().String())
	hash := sha1.New()
	hash.Write(now)

	return hex.EncodeToString(hash.Sum(nil))
}

var releaseTagRegex = regexp.MustCompile(`^v[0-9]+\.[0-9]+\.[0-9]+?\z`)

// Push pushes a docker image to a registry. If the image has a release tag, it will push the image twice:
// once with the release tag and once with a unique hash.
// This is done to ensure that we can always restore a previously built release image in emergency.
func (Docker) Push(image string) error {
	docker := newDocker()

	parts := strings.Split(image, ":")
	if len(parts) != 2 {
		return docker.Push(image)
	}

	tag := parts[1]
	if !releaseTagRegex.MatchString(tag) {
		// push non-release tags as normal
		return docker.Push(image)
	}

	// check if the image already exists
	// at this point we know this is a release tag
	// we don't want to push if the image already exists
	if err := docker.Inspect(image); err == nil {
		fmt.Printf("Docker image %q already exists, skipping push\n", image)
		return nil
	}

	// create an image with a unique hash, so we can always restore a previously built release image in emergency
	for _, img := range []string{image, fmt.Sprintf("%s-%s", image, newHash())} {
		fmt.Printf("Pushing release image: %q\n", img)
		if err := docker.Push(img); err != nil {
			return err
		}
	}

	return nil
}
