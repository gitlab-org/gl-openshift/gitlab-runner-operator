package main

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPush(t *testing.T) {
	tests := map[string]struct {
		image string

		shouldCallPush bool // whether Push should be called
		err            bool // whether Push is expected to return an error

		shouldCallPushWithHash bool // whether Push should also be called with a hash in case of a release tag

		shouldCallInspect bool // whether Inspect should be called. Inspect should be called only for release tags
		inspectErr        bool // whether Inspect is expected to return an error. If an error is returned it's presumed it's because the image does not exist
	}{
		"invalid image": {
			image: "invalid",

			shouldCallPush: true,
			err:            true,

			shouldCallPushWithHash: false,

			shouldCallInspect: false,
			inspectErr:        false,
		},
		"valid image but not a tag": {
			image: "valid:latest",

			shouldCallPush: true,
			err:            false,

			shouldCallPushWithHash: false,

			shouldCallInspect: false,
			inspectErr:        false,
		},
		"valid tag image does not exist": {
			image: "valid:v1.0.0",

			shouldCallPush: true,
			err:            false,

			shouldCallPushWithHash: true,

			shouldCallInspect: true,
			inspectErr:        true,
		},
		"valid tag image exists": {
			image: "valid:v1.0.0",

			shouldCallPush: false,
			err:            false,

			shouldCallPushWithHash: false,

			shouldCallInspect: true,
			inspectErr:        false,
		},
		"tag image with suffix should just push": {
			image: "valid:v1.0.0-rc1",

			shouldCallPush: true,
			err:            false,

			shouldCallPushWithHash: false,

			shouldCallInspect: false,
			inspectErr:        false,
		},
	}

	for tn, tc := range tests {
		t.Run(tn, func(t *testing.T) {
			newHash = func() string {
				return tn
			}

			mockDocker := newMockDockerInterface(t)

			if tc.shouldCallPush {
				if tc.err {
					mockDocker.On("Push", tc.image).Return(errors.New("err-probably-invalid-image"))
				} else {
					mockDocker.On("Push", tc.image).Return(nil)
				}
			}

			if tc.shouldCallPushWithHash {
				mockDocker.On("Push", tc.image+"-"+tn).Return(nil)
			}

			if tc.shouldCallInspect {
				if tc.inspectErr {
					mockDocker.On("Inspect", tc.image).Return(errors.New("err-probably-does-not-exist"))
				} else {
					mockDocker.On("Inspect", tc.image).Return(nil)
				}
			}

			defer mockDocker.AssertExpectations(t)

			newDocker = func() dockerInterface {
				return mockDocker
			}

			err := Docker{}.Push(tc.image)
			if tc.err {
				assert.Errorf(t, err, "expected error")
			} else {
				assert.NoError(t, err, "unexpected error")
			}
		})
	}
}
